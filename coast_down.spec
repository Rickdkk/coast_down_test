# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['coast_down.py'],
             pathex=['C:\\Users\\rick_\\Development\\coast_down2'],
             binaries=[],
             datas=[("mainwindow_coast_down.ui", "."), ("mainwindow_coast_down.py", "."), ("accessibility_icon_small.png", ".")],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='coast_down_test',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False,
          icon="accessibility_icon_small.ico")
