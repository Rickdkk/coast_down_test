import copy
import os
import sys

os.environ['QT_API'] = 'pyside2'

import pyqtgraph as pg
import qdarkstyle as qdarkstyle

from PySide2 import QtWidgets
from PySide2.QtGui import QIcon
from numpy.polynomial.polynomial import polyfit
import numpy as np

import worklab as wl

from mainwindow_coast_down import Ui_MainWindow


class ApplicationWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(ApplicationWindow, self).__init__()
        self.setupUi(self)
        self.setWindowIcon(QIcon(self.resource_path("accessibility_icon_small.png")))
        self.pb_Import.setFocus()  # selects pb when you press enter
        self.plotItem = self.graphicsView.getPlotItem()
        self.region, self.plt, self.filename, self.data = None, None, None, None
        self.masks, self.iter_labels, self.decelerations, self.iter_boxes, self.index = [], [], [], [], 0

        self.make_iterables()
        self.initialize_plot()
        self.initialize_connections()
        self.statusBar().showMessage("Application Ready, please import some data and set user parameters!")

    def initialize_connections(self):
        """Connect all buttons and comboboxes to their respective functions"""
        self.cb_Selected.currentIndexChanged.connect(self.plot_variable)
        self.pb_Grab.clicked.connect(self.grab_selection)
        self.pb_Calc.clicked.connect(self.calculate_outcomes)
        self.pb_Reset.clicked.connect(self.reset)
        self.pb_Export.clicked.connect(self.export)
        self.pb_Import.clicked.connect(self.import_data)

    def initialize_plot(self):
        """Initialize plots and fancy labels"""
        self.plotItem.layout.setContentsMargins(5, 0, 0, 15)  # make sure labels don't clip
        self.plotItem.setTitle("Coast-down data from measurement wheel")
        self.plotItem.setLabel(axis="bottom", text="Time (s)")
        self.plotItem.setLabel(axis="left", text="Speed")
        self.plotItem.showGrid(x=True, y=True)
        self.plotItem.enableAutoRange(axis="xy", enable=True)

    def make_iterables(self):
        """Make sure we can iterate through the spinboxes"""
        self.iter_boxes = [self.sb_Dec0, self.sb_Dec1, self.sb_Dec2, self.sb_Dec3, self.sb_Dec4, self.sb_Dec5,
                           self.sb_Dec6, self.sb_Dec7, self.sb_Dec8, self.sb_Dec9]
        self.iter_labels = [self.label_Mu, self.label_Friction, self.label_N, self.label_Dec]

    def import_data(self):
        """Import coast-down data from optipush wheel"""
        file_filter = "MW/NGIMU/WE (*.data; *.txt; *.dat; *.xml; *.xls)"
        (self.filename, _) = QtWidgets.QFileDialog.getOpenFileName(self, "Open Measurement Wheel file",
                                                                   filter=file_filter)
        if not self.filename:
            return  # escape if there is nothing to read

        wheelsize = self.sb_Wheeldiam.value() / 2
        camber = self.sb_Camber.value()

        if ".data" in self.filename:
            self.data = wl.io.load_opti(self.filename)
            self.data = wl.calcs.filter_data(self.data, co_s=5, ord_s=8)
            self.data = wl.calcs.process_data(self.data, wheelsize=wheelsize, sfreq=200)
        elif ".xml" in self.filename:
            item, ok_pressed = QtWidgets.QInputDialog.getItem(self, "Config", "Select NGIMU configuration:",
                                                              ("In wheel", "On wheel"), 0, False)
            if not ok_pressed:
                return

            data = wl.io.load_session(os.path.split(self.filename)[0], filenames=["sensors"])
            data = wl.calcs.resample_imu(data, 200)

            if item == "On wheel":  # remap data to 'correct' axes
                datacopy = copy.deepcopy(data)
                data["Left"]["sensors"]["GyroscopeZ"] = datacopy["Left"]["sensors"]["GyroscopeX"]
                data["Left"]["sensors"]["GyroscopeY"] = datacopy["Left"]["sensors"]["GyroscopeZ"]
                data["Left"]["sensors"]["GyroscopeX"] = datacopy["Left"]["sensors"]["GyroscopeY"]

                data["Right"]["sensors"]["GyroscopeZ"] = datacopy["Right"]["sensors"]["GyroscopeX"]
                data["Right"]["sensors"]["GyroscopeY"] = datacopy["Right"]["sensors"]["GyroscopeZ"] * -1
                data["Right"]["sensors"]["GyroscopeX"] = datacopy["Right"]["sensors"]["GyroscopeY"]

            data = wl.calcs.calc_wheelspeed(data, camber=camber, wsize=wheelsize)
            self.data = {"time": data["Frame"]["sensors"]["Time"].values,
                         "speed": data["Frame"]["sensors"]["CombSkidVel"].values}
            self.data["speed"] = wl.calcs.lowpass_butter(self.data["speed"], sfreq=200, co=10)
        elif ".xls" in self.filename:
            item, ok_pressed = QtWidgets.QInputDialog.getItem(self, "Config", "Select wheel configuration:",
                                                              ("Left wheel", "Right wheel", "Mean"), 0, False)
            if not ok_pressed:
                return
            data = wl.io.load_LEM(self.filename)
            data = wl.calcs.filter_data(data, 100, co_s=5, ord_s=8)
            data = wl.calcs.process_data(data, wheelsize=wheelsize)
            if item == "Left wheel":
                self.data = data["left"]
            elif item == "Right wheel":
                self.data = data["right"]
            elif item == "Mean":
                data["left"]["speed"] = data["left"]["speed"][:len(data["right"]["speed"])]
                speed = np.mean([data["left"]["speed"], data["right"]["speed"]], axis=0)
                self.data = {"time": data["left"]["time"], "speed": speed}

        self.pb_Grab.setEnabled(True)
        self.reset()
        self.statusBar().showMessage(f"Data from {self.filename}")

    def plot_variable(self):
        """Plot selected variable if data is present"""
        if not np.any(self.data):
            return  # escape if there is nothing to plot
        variable = self.cb_Selected.currentText()
        self.plotItem.setLabel(axis="left", text=variable)
        if variable.lower() not in self.data:
            variable = "speed"
            self.cb_Selected.setCurrentIndex(0)
        self.plotItem.plot(self.data["time"], self.data[variable.lower()], clear=True)
        self.region = pg.LinearRegionItem(values=(10, 20), bounds=[0, self.data["time"][-1]])
        self.plotItem.addItem(self.region)
        self.plot_segments()

    def grab_selection(self):
        """Grab selection from plot and create mask on data"""
        if self.index > 9:
            self.statusBar().showMessage("Already selected all coast-down moments!")
            return
        self.pb_Calc.setEnabled(True)
        self.pb_Reset.setEnabled(True)
        start, stop = self.region.getRegion()
        mask = np.nonzero(((self.data["time"] > start) & (self.data["time"] < stop) &
                           (self.data["speed"] > self.sb_MinSpeed.value()) &
                           (self.data["speed"] < self.sb_MaxSpeed.value())))
        if mask[0].any():
            start = mask[0][0]
            stop = mask[0][-1] + 1
            self.masks.append([start, stop])
            deceleration = polyfit(self.data["time"][start:stop], self.data["speed"][start:stop], 1)
            self.decelerations.append(deceleration)
            self.iter_boxes[self.index].setValue(abs(deceleration[1]))
            self.index = self.index + 1
            self.statusBar().showMessage(f"Selected data between {self.data['time'][start]:.2f}s and "
                                         f"{self.data['time'][stop]:.2f}s")
            self.plot_segments()

    def plot_segments(self):
        """Plots all segments that have been selected as a red line on top of graph"""
        for mask, dec in zip(self.masks, self.decelerations):
            self.plotItem.plot(self.data["time"][mask[0]:mask[1]],
                               self.data[self.cb_Selected.currentText().lower()][mask[0]:mask[1]], pen="r")
            self.plotItem.plot(self.data["time"][mask[0]:mask[1]],
                               self.data["time"][mask[0]:mask[1]] * dec[1] + dec[0], pen="y")

    def calculate_outcomes(self):
        """Calculate friction from decelerations with f=m*a and convert to mu value for ergometer"""
        final_decelerations = []
        [final_decelerations.append(box.value()) for box in self.iter_boxes]
        final_decelerations = np.array(final_decelerations)
        final_decelerations = final_decelerations[final_decelerations > 0]  # only use boxes with nonzero values
        total_weight = self.sb_WWeight.value() + self.sb_PWeight.value() + self.sb_Inertia.value()
        weight = self.sb_WWeight.value() + self.sb_PWeight.value()
        mu = (total_weight * final_decelerations.mean()) / (weight * 9.81)
        self.label_N.setText(f"{len(final_decelerations)}")
        self.label_Dec.setText(f"{final_decelerations.mean():.2f}")
        self.label_Friction.setText(f"{total_weight * final_decelerations.mean():.2f}")
        self.label_Mu.setText(f"{mu:.3f}")
        self.pb_Export.setEnabled(True)
        self.statusBar().showMessage(f"Calculated friction for {len(final_decelerations)} coast-downs!")

    def reset(self):
        """Reset all selections, outcomes, and plot"""
        [box.setValue(0.0) for box in self.iter_boxes]
        [label.setText(".......") for label in self.iter_labels]
        self.decelerations = []
        self.masks = []
        self.index = 0
        self.pb_Calc.setEnabled(False)
        self.pb_Reset.setEnabled(False)
        self.pb_Export.setEnabled(False)
        self.plot_variable()
        self.statusBar().showMessage(f"All selections are reset!")

    def export(self):
        """Export data to .txt file so results from coast-down can be reproduced"""
        exportname = self.filename.split(".data")[0] + "_CD.txt"
        with open(exportname, "w") as f:
            f.write(f"Coast-down selection information for {self.filename}\nDecelerations [m/s2]: \t")
            for dec in self.decelerations:
                f.write(f"{dec}\t")
            f.write("\nStart/stop [idx]: \t")
            for mask in self.masks:
                f.write(f"{mask}\t")
            f.write(f"\nWheelchair weight [kg]: \t{self.sb_WWeight.value()}")
            f.write(f"\nParticipant weight [kg]: \t{self.sb_PWeight.value()}")
            f.write(f"\nWheels inertia [kg]: \t{self.sb_Inertia.value()}")
            f.write(f"\nMinspeed [m/s]: \t{self.sb_MinSpeed.value()}")
            f.write(f"\nMaxspeed [m/s]: \t{self.sb_MaxSpeed.value()}")
            f.write(f"\nIncluded coast downs [n]: \t{self.label_N.text()}")
            f.write(f"\nMean dec [m/s2]: \t{self.label_Dec.text()}")
            f.write(f"\nMean frict [N]: \t{self.label_Friction.text()}")
            f.write(f"\nRolling frict coeff [mu]: \t{self.label_Mu.text()}")
        self.statusBar().showMessage(f"Exported to {exportname}!")

    @staticmethod
    def resource_path(relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except AttributeError:
            base_path = os.path.abspath(".")
        return os.path.join(base_path, relative_path)


def main():
    app = QtWidgets.QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet())
    application = ApplicationWindow()
    application.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
